#!/usr/bin/env bash
#
# Install application software on the inVADER SIC
#

: ${PREFIX=/usr/local}
: ${BINDIR=$PREFIX/bin}

export BINDIR

VERSION="v0.2.0"

IA_REPO=bitbucket.org/uwaploe/imagerpc.git
IA_VERSION=v0.8.1

declare -a APPS
APPS=(
    "https://bitbucket.org/uwaploe/pdumon/downloads/pdumon_0.9.2_amd64.deb"
    "https://bitbucket.org/uwaploe/pduweb/downloads/pduweb_0.3.2_amd64.deb"
    "https://bitbucket.org/uwaploe/rdarchiver/downloads/rdarchiver_1.6.0_amd64.deb"
    "https://bitbucket.org/uwaploe/camctl/downloads/camctl_0.9.2_amd64.deb"
    "https://bitbucket.org/uwaploe/diorpc/downloads/dio-server_0.6.0_amd64.deb"
)
SYSFILES="https://bitbucket.org/uwaploe/invader-sys/downloads/invader-systemd-${VERSION}.tar.gz"
BINFILES="https://bitbucket.org/uwaploe/invader-sys/downloads/invader-utils-${VERSION}.tar.gz"

set -o errexit

# Install Image Acquisition server package
pip3 install git+https://${IA_REPO}@${IA_VERSION}#egg=imagesvc

tmpdir="${TMPDIR:-/tmp}"
declare -a tmpdirs
function onexit
{
    for d in "${tmpdirs[@]}"; do
        parent="${d%/*}"
        [[ $parent = $tmpdir ]] && rm -rf "$d"
    done
}
trap onexit EXIT

# Install apps
for url in "${APPS[@]}"; do
    d="$(mktemp -d)"
    tmpdirs+=( "$d" )
    case "$url" in
        *.tar.gz)
            curl -fsSL "$url" | tar --strip-components=1 -xzvf - -C "$d"
            if [[ -d $d/CONTROL ]] && [[ -x $d/CONTROL/install ]]; then
                # Run install script
                $d/CONTROL/install
            else
                # Copy all executables to /usr/local/bin
                for $f in $d/*; do
                    if [[ -f $f ]] && [[ -x $f ]]; then
                        cp -v "$f" "$BINDIR"
                    fi
                done
            fi
            ;;
        *.deb)
            (
                cd "$d"
                curl -fsSL -O "$url" && dpkg -i "${url##*/}"
            )
            ;;
    esac
done


if [[ -n $SYSFILES ]]; then
    curl -fsSL "$SYSFILES" | tar -xzvf - -C /etc/systemd
    systemctl daemon-reload
fi

if [[ -n $BINFILES ]]; then
    curl -fsSL "$BINFILES" | tar -xzvf - -C "$BINDIR"
fi
