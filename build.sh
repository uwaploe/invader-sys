#!/usr/bin/env bash

vers="$(git describe --tags --always --dirty --match=v* 2> /dev/null)"
VERSION="${vers#v}"
export VERSION
nfpm pkg --target invader-sys_${VERSION}_amd64.deb
